package com.example.todoapp.data

import androidx.room.TypeConverter
import com.example.todoapp.data.model.Priority

class Converter {

    //mempertahankan tipe kostum kedalam database
    @TypeConverter
    fun fromPriority(priority: Priority): String{
        return priority.name
    }

    @TypeConverter
    fun toPriority(priority: String): Priority {
        return Priority.valueOf(priority)
    }
}