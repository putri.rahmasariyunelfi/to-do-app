package com.example.todoapp.data.model

enum class Priority {
    //priority
    HIGH,
    MEDIUM,
    LOW
}